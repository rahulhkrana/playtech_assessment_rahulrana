package com.playtech.page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlaytechHomePg extends LoadableComponent<PlaytechHomePg>{	
	
	WebDriver driver;
	
	public PlaytechHomePg(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "header-site-logo")
	@CacheLookup
	WebElement homePgHeaderLogo;
	
	@FindBy(how = How.XPATH, using = "//div[1]/span[normalize-space()='Menu']")
	@CacheLookup
	WebElement homePgMenuLink;
	
	@FindBy(how = How.XPATH, using = "//a/child::span[normalize-space()='About Us']")
	@CacheLookup
	WebElement homePgAboutUsLink;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"close-cookie\"]")
	@CacheLookup
	WebElement homePgCloseCookiesBtn;
	
	
	
	public boolean isHomePgHeaderLogoAvailable() {
		waitForVisibility(homePgHeaderLogo);
		return homePgHeaderLogo.isDisplayed();
	}
	
	public boolean isHomePgMenuAvailable() {
		waitForVisibility(homePgMenuLink);
		return homePgMenuLink.isDisplayed();
	}
	
	public void navigateToAboutUsPg() {		
		homePgMenuLink.click();
		waitForVisibility(homePgAboutUsLink);
		homePgAboutUsLink.click();		
	}
	
	public void closeCookiesBtn() {
		waitForVisibility(homePgCloseCookiesBtn);
		homePgCloseCookiesBtn.click();
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	
	private void waitForVisibility(WebElement element) throws Error {
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
	}

}
