package com.playtech.page.objects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AboutUsPg extends LoadableComponent<AboutUsPg>{
	
	WebDriver driver;
	
	public AboutUsPg(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div/h1[normalize-space()='About Us']")
	@CacheLookup
	WebElement aboutUsPgHeadLn;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ccm-block statsBlock']")
	@CacheLookup
	WebElement aboutUsPgStatsSection;
	
	@FindBy(how = How.XPATH, using = "//div[normalize-space()='Number of Employees']/parent::div/div")
	@CacheLookup
	WebElement NumOfEmpStats;
	
	@FindBy(how = How.XPATH, using = "//div[normalize-space()='Number of countries Playtech has offices']/parent::div/div")
	@CacheLookup
	WebElement NumOfCountriesPlaytechOfficesStats;
	
	@FindBy(how = How.XPATH, using = "//div[normalize-space()='Global licensees']/parent::div/div")
	@CacheLookup
	WebElement NumOfGloballicenseesStats;
	
	@FindBy(how = How.XPATH, using = "//div[normalize-space()='Regulated Jurisdictions']/parent::div/div")
	@CacheLookup
	WebElement NumRegulatedJurisdictionsStats;
	
	public boolean isAboutUsPgLoaded() {
		waitForVisibility(aboutUsPgHeadLn);		
		return aboutUsPgHeadLn.isDisplayed();
	}
	
	public boolean scrollToAboutUsStats() throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", aboutUsPgStatsSection);
		Thread.sleep(5000);
		waitForVisibility(aboutUsPgStatsSection);
		return aboutUsPgStatsSection.isDisplayed();
	}
	
	public String getNumOfEmpStats() {
		return NumOfEmpStats.getAttribute("data-odometer-count");
	}
	
	public String getNumOfCountriesPlaytechOfficesStats() {
		return NumOfCountriesPlaytechOfficesStats.getAttribute("data-odometer-count");
	}
	
	public String getNumOfGloballicenseesStats() {
		return NumOfGloballicenseesStats.getAttribute("data-odometer-count");
	}
	
	public String getNumRegulatedJurisdictionsStats() {
		return NumRegulatedJurisdictionsStats.getAttribute("data-odometer-count");
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	
	private void waitForVisibility(WebElement element) throws Error {
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
	}

}
