package com.playtech.page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.playtech.constants.Constant;
import com.playtech.utilities.*;

public class ConfirmLegalAgePg extends LoadableComponent<ConfirmLegalAgePg> {

	WebDriver driver;
	DOBCalculator dobCalculator;

	public ConfirmLegalAgePg(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div[@class=\"content\"]/child::h3[normalize-space()='" + Constant.WELCOME_MSG
			+ "']")
	@CacheLookup
	WebElement welcomeTxt;

	@FindBy(how = How.XPATH, using = "//div[@class=\"content\"]/child::h3[normalize-space()='"
			+ Constant.PLEASE_CONFIRM_LEGAL_AGE_MSG + "']")
	@CacheLookup
	WebElement confirmLegalAgeTxt;

	@FindBy(how = How.NAME, using = "day")
	@CacheLookup
	WebElement LegalAgeDate;

	@FindBy(how = How.NAME, using = "month")
	@CacheLookup
	WebElement LegalAgeMonth;

	@FindBy(how = How.NAME, using = "year")
	@CacheLookup
	WebElement LegalAgeYear;

	@FindBy(how = How.XPATH, using = "//button[normalize-space()='Enter Site']")
	@CacheLookup
	WebElement EnterSiteBtn;
	
	@FindBy(how = How.XPATH, using = "//div/strong")
	@CacheLookup
	WebElement AlertInitials;
	
	@FindBy(how = How.XPATH, using = "//div/child::span[normalize-space()='Please enter a valid date of birth.']")
	@CacheLookup
	WebElement AlertToEnterValidDOB;
	
	@FindBy(how = How.XPATH, using = "//div/child::span[normalize-space()='Sorry you must be over 18 to enter.']")
	@CacheLookup
	WebElement AlertToEnterDOBOverEighteen;
	
	

	public void navigateTo_Playtech(String environmentToTest) {
		switch (environmentToTest) {
		case "local":
			System.out.println("Test Running on : " + environmentToTest);
			driver.get(Constant.PLAYTECH_LOCAL_URL);
			break;
		case "cit":
			System.out.println("Test Running on : " + environmentToTest);
			driver.get(Constant.PLAYTECH_CIT_URL);
			break;
		case "sit":
			System.out.println("Test Running on : " + environmentToTest);
			driver.get(Constant.PLAYTECH_SIT_URL);
			break;
		case "uat":
			System.out.println("Test Running on : " + environmentToTest);
			driver.get(Constant.PLAYTECH_UAT_URL);
			break;
		}
	}

	public String isLegalAgePageOpened() {
		waitForVisibility(welcomeTxt);
		return welcomeTxt.getText();
	}

	public String pleaseConfirmLegalAgeMsg() {
		waitForVisibility(confirmLegalAgeTxt);
		return confirmLegalAgeTxt.getText();
	}

	@SuppressWarnings("static-access")
	public void SelectDOBDate(String AgeInYears) throws CustomException {
		try {
			waitForVisibility(LegalAgeDate);
			Select drop = new Select(LegalAgeDate);
			drop.selectByValue(dobCalculator.getDOBDate(AgeInYears));
		} catch (Exception e) {
			throw new CustomException("Issue with Legal Age Date Selection");
		}
	}

	@SuppressWarnings("static-access")
	public void SelectDOBMonth(String AgeInYears) throws CustomException {
		try {
			waitForVisibility(LegalAgeMonth);
			Select drop = new Select(LegalAgeMonth);
			drop.selectByValue(dobCalculator.getDOBMonth(AgeInYears));
		} catch (Exception e) {
			throw new CustomException("Issue with Legal Age Month Selection");
		}
	}

	@SuppressWarnings("static-access")
	public void SelectDOBYear(String AgeInYears) throws CustomException {
		try {
			waitForVisibility(LegalAgeYear);
			Select drop = new Select(LegalAgeYear);
			drop.selectByValue(dobCalculator.getDOBYear(AgeInYears));			
		} catch (Exception e) {
			throw new CustomException("Issue with Legal Age Year Selection");
		}
	}

	public void EnterPlaytechSiteBtn() {
		waitForVisibility(EnterSiteBtn);
		EnterSiteBtn.click();
	}
	
	public String isAlertToEnterValidDOBVisible() {
		waitForVisibility(AlertToEnterValidDOB);
		return AlertInitials.getText() + " " + AlertToEnterValidDOB.getText();
	}
	
	public String isAlertToEnterDOBOverEighteenVisible() {
		waitForVisibility(AlertToEnterDOBOverEighteen);
		return AlertInitials.getText() + " " + AlertToEnterDOBOverEighteen.getText();
	}
	

	@Override
	protected void load() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

	private void waitForVisibility(WebElement element) throws Error {
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
	}

}
