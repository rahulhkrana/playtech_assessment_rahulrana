package com.playtech.stepDefinitions;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.playtech.managers.BrowserManager;
import com.playtech.testContext.TestContext;

import io.cucumber.java.*;

public class Hooks {

	TestContext testContext;
	public BrowserManager browserManager;

	public Hooks(TestContext context) {
		testContext = context;
	}

	@Before
	public void BeforeSteps() {

	}

	@SuppressWarnings("deprecation")
	@BeforeStep
	public void BeforeStep(Scenario scenario) {
		try {
			byte[] screenshot = ((TakesScreenshot)testContext.getBrowserManager().getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");  // Stick it in the report
		} catch (Exception e) {
			System.out.println("Error while embedding Screenshot to the Report : " + e.getMessage());
		}
	}

	@AfterStep
	public void doSomethingAfterStep() {

	}

	@After
	public void AfterSteps() {
		testContext.getBrowserManager().closeDriver(System.getProperty("browser"));
	}

}
