package com.playtech.stepDefinitions;

import static org.testng.Assert.*;

import org.testng.asserts.SoftAssert;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import com.playtech.testContext.TestContext;
import com.playtech.utilities.CustomException;
import com.playtech.constants.Constant;
import com.playtech.page.objects.AboutUsPg;
import com.playtech.page.objects.ConfirmLegalAgePg;
import com.playtech.page.objects.PlaytechHomePg;

public class PlaytechSteps{

	TestContext testContext;	
	SoftAssert softAssertion;
	ConfirmLegalAgePg confirmLegalAgePg;
	PlaytechHomePg playtechHomePg;
	AboutUsPg aboutUsPg;
	
	//-Globally declared variables
	public String environmentUnderTest;	
	public String LegalAgeInYears;
	
	public PlaytechSteps(TestContext context) {
		
		testContext = context;
		confirmLegalAgePg = testContext.getPageObjectManager().getConfirmLegalAgePg();
		playtechHomePg = testContext.getPageObjectManager().getPlaytechHomePg();
		aboutUsPg = testContext.getPageObjectManager().getAboutUsPg();
		environmentUnderTest = System.getProperty("environment");
	}

	@Given("User opens Playtech website in browser")
	public void user_opens_Playtech_website_in_browser() {
		
		confirmLegalAgePg.navigateTo_Playtech(environmentUnderTest);
		assertTrue(confirmLegalAgePg.isLegalAgePageOpened().equalsIgnoreCase(Constant.WELCOME_MSG));
	}

	@When("User is being asked to confirm the legal {int} before entering site")
	public void user_is_being_asked_to_confirm_the_legal_before_entering_site(Integer UserLegalAge) {
		
		LegalAgeInYears = UserLegalAge.toString();
		assertTrue(confirmLegalAgePg.pleaseConfirmLegalAgeMsg().equalsIgnoreCase(Constant.PLEASE_CONFIRM_LEGAL_AGE_MSG));
	}

	@When("user selects DOB {string} from the dropdown")
	public void user_selects_DOB_from_the_dropdown(String dobSelector) throws CustomException {
		if (LegalAgeInYears.equalsIgnoreCase("0")) {
			System.out.println("No action on selection DOB from the drop downs");
		} else {
			switch (dobSelector) {
			case "day":
				confirmLegalAgePg.SelectDOBDate(LegalAgeInYears);
				break;
			case "month":
				confirmLegalAgePg.SelectDOBMonth(LegalAgeInYears);
				break;
			case "year":
				confirmLegalAgePg.SelectDOBYear(LegalAgeInYears);
				break;
			}
		}
	}

	@When("user clicks on {string} button")
	public void user_clicks_on_button(String string) {

		confirmLegalAgePg.EnterPlaytechSiteBtn();
	}

	@Then("User is being redirected to Playtech Home screen")
	public void user_is_being_redirected_to_Playtech_Home_screen() {

		playtechHomePg.closeCookiesBtn();
		assertTrue(playtechHomePg.isHomePgHeaderLogoAvailable());
		assertTrue(playtechHomePg.isHomePgMenuAvailable());
	}

	@When("user navigates to {string} page")
	public void user_navigates_to_page(String string) {
	    // Write code here that turns the phrase above into concrete actions
		playtechHomePg.navigateToAboutUsPg();
		assertTrue(aboutUsPg.isAboutUsPgLoaded());
	}

	@Then("user should validate the {string} successfully")
	public void user_should_validate_the_successfully(String AboutUsStats) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		assertTrue(aboutUsPg.scrollToAboutUsStats());
		switch (AboutUsStats) {
		case "Number of Employees":
			assertTrue(aboutUsPg.getNumOfEmpStats().equalsIgnoreCase(Constant.NUMBER_OF_EMPLOYEES));
			break;
		case "Number of countries Playtech has offices":
			assertTrue(aboutUsPg.getNumOfCountriesPlaytechOfficesStats()
					.equalsIgnoreCase(Constant.NUMBER_OF_COUNTRIES_PLAYTECH_HAS_OFFICES));
			break;
		case "Global licensees":
			assertTrue(aboutUsPg.getNumOfGloballicenseesStats().equalsIgnoreCase(Constant.GLOBAL_LICENSEES));
			break;
		case "Regulated Jurisdictions":
			assertTrue(
					aboutUsPg.getNumRegulatedJurisdictionsStats().equalsIgnoreCase(Constant.REGULATED_JURISDICTIONS));
			break;
		}
	}
	
	@Then("user should get an error {string} on the screen")
	public void user_should_get_an_error_on_the_screen(String AlertMsg) {

		if (LegalAgeInYears.equalsIgnoreCase("0")) {
			assertTrue(confirmLegalAgePg.isAlertToEnterValidDOBVisible().equalsIgnoreCase(AlertMsg));
		}else {
			assertTrue(confirmLegalAgePg.isAlertToEnterDOBOverEighteenVisible().equalsIgnoreCase(AlertMsg));
		}
	}

	@Then("User is not redirected to Playtech Home screen")
	public void user_is_not_redirected_to_Playtech_Home_screen() {

		assertTrue(confirmLegalAgePg.isLegalAgePageOpened().equalsIgnoreCase(Constant.WELCOME_MSG));
		assertTrue(confirmLegalAgePg.pleaseConfirmLegalAgeMsg().equalsIgnoreCase(Constant.PLEASE_CONFIRM_LEGAL_AGE_MSG));
	}
}
