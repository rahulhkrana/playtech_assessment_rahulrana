package com.playtech.managers;

import org.openqa.selenium.WebDriver;

import com.playtech.page.objects.AboutUsPg;
import com.playtech.page.objects.ConfirmLegalAgePg;
import com.playtech.page.objects.PlaytechHomePg;

public class PageObjectManager {

	private WebDriver driver;
	private ConfirmLegalAgePg confirmLegalAgePg;
	private PlaytechHomePg playtechHomePg;
	private AboutUsPg aboutUsPg;
	
	public PageObjectManager(WebDriver driver) {

		this.driver = driver;

	}

	public ConfirmLegalAgePg getConfirmLegalAgePg() {

		return (confirmLegalAgePg == null) ? confirmLegalAgePg = new ConfirmLegalAgePg(driver) : confirmLegalAgePg;

	}
	
	public PlaytechHomePg getPlaytechHomePg() {

		return (playtechHomePg == null) ? playtechHomePg = new PlaytechHomePg(driver) : playtechHomePg;

	}
	
	public AboutUsPg getAboutUsPg() {

		return (aboutUsPg == null) ? aboutUsPg = new AboutUsPg(driver) : aboutUsPg;

	}

}