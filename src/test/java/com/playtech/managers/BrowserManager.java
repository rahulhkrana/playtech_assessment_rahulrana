package com.playtech.managers;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserManager {

	private WebDriver driver;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	private static final String IE_DRIVER_PROPERTY = "webdriver.ie.driver";

	// Constructor for BrowserFactory
	public BrowserManager() {
	}
	
	public WebDriver getDriver() {
		if (driver == null)
			driver = getLocalDriver(System.getProperty("browser"));
		return driver;
	}

	private WebDriver getLocalDriver(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			System.out.println("in chrome");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notification");
			options.setPageLoadStrategy(PageLoadStrategy.NONE);
			/*
			 * -For Headless Mode options.addArguments("headless");
			 * options.addArguments("start-maximized"); -For Headless Mode
			 */
			System.setProperty(CHROME_DRIVER_PROPERTY, System.getProperty("user.dir") + "//Drivers/chromedriver.exe");
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.out.println("in firefox");
			System.setProperty(FIREFOX_DRIVER_PROPERTY, System.getProperty("user.dir") + "//Drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
		} else if (browserName.equalsIgnoreCase("IE")) {
			System.setProperty(IE_DRIVER_PROPERTY, System.getProperty("user.dir") + "//Drivers/IEDriverServer.exe");
			// -Used if you are behind Proxy
			// DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			// cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
			// true);
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
		}
		return driver;
	}

	public void closeDriver(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			driver.close();
			driver.quit();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			driver.close();
		} else if (browserName.equalsIgnoreCase("IE")) {
			driver.quit();
		}
	}

}
