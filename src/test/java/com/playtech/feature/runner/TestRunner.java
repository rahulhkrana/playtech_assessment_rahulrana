package com.playtech.feature.runner;

import org.testng.annotations.BeforeTest;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

/*
	-Cucumber Tag(s) Classifications:
		- @Regression 			: To Run all available test(s)
		- @Playtech-AboutUs		: To Run test(s) on AboutUs Page
		- @Playtech-LegalAge    : To Run test(s) on Legal Age Page

*/
@CucumberOptions(features = "src/test/resources",
					glue = { "com.playtech.stepDefinitions" },
					//objectFactory = FooFactory.class, //- Specify an object factory - if you are using Cucumber with a DI framework and want to use a custom object factory
					//strict = false,  //- Skip undefined tests - if you want to skip undefined steps from execution
					//dryRun=true,     //- Performing a dry-run - if you want to check whether all feature file steps have corresponding step definitions
					monochrome = true, //- Formatting console output - if you want console output from Cucumber in a readable format
					tags = {"~", "not @bar" },
					dryRun = false,
					plugin = {"pretty",
							   "html:target/cucumber-reports/cucumber-pretty",
					          "json:target/cucumber-reports/CucumberTestReport.json"			           
					          })
public class TestRunner extends AbstractTestNGCucumberTests{
	
	@BeforeTest
	public String BrowserToRun() {
		String browserToTest = System.getProperty("browser");
		String envToTest = System.getProperty("environment");
		if(browserToTest==null && envToTest==null) {
			//- browser can be chrome, firefox, IE etc
			browserToTest = "chrome";
			//- envToTest can be local, cit, sit, uat etc
			envToTest = "local";
			System.setProperty("browser", browserToTest);
			System.setProperty("environment", envToTest);
		}		
		return browserToTest;
	}
}