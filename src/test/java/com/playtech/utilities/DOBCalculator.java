package com.playtech.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DOBCalculator {
	
	static String DOB;

	public static String getDOBDate(String CurAgeInYears) {
		DOB = getFullDOB(Integer.parseInt(CurAgeInYears));
		String DOBDate = DOB.split("-")[0]; 
		return DOBDate;		
	}
	
	public static String getDOBMonth(String CurAgeInYears) {
		DOB = getFullDOB(Integer.parseInt(CurAgeInYears));
		String DOBMonth = DOB.split("-")[1]; 
		return DOBMonth;		
	}
	
	public static String getDOBYear(String CurAgeInYears) {
		DOB = getFullDOB(Integer.parseInt(CurAgeInYears));
		String DOBYear = DOB.split("-")[2]; 
		return DOBYear;		
	}
	
	private static String getFullDOB(int AgeInYears) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        //System.out.println("Current Date " + dateFormat.format(date));

        // Convert Date to Calendar
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        // Perform addition/subtraction
        c.add(Calendar.YEAR, -AgeInYears);
        
        // Convert calendar back to Date
        Date currentDatePlusOne = c.getTime();
        System.out.println("Age in Years  : " + AgeInYears);
        System.out.println("Date of Birth : " + dateFormat.format(currentDatePlusOne));
		
		return dateFormat.format(currentDatePlusOne);
		
	}

}
