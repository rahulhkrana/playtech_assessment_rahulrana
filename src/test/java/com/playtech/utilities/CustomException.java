package com.playtech.utilities;

@SuppressWarnings("serial")
public class CustomException extends Exception {
	public CustomException(String CustomExceptionString) {
		super(CustomExceptionString);
	}
}