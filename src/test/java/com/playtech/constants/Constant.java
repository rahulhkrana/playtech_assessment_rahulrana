package com.playtech.constants;

public class Constant {
	 
	 //- Environment Specific Urls to run the test(s)
	 public static final String PLAYTECH_LOCAL_URL = "https://www.playtech.com";
	 public static final String PLAYTECH_CIT_URL = "https://www.playtech.com";
	 public static final String PLAYTECH_SIT_URL = "https://www.playtech.com";
	 public static final String PLAYTECH_UAT_URL = "https://www.playtech.com";
	 
	 
	 //- Legal Age Input Page
	 public static final String WELCOME_MSG = "Welcome to the Playtech website";
	 public static final String PLEASE_CONFIRM_LEGAL_AGE_MSG = "Please confirm you are of legal age before entering this site";
	 public static final String ERROR_MSG_FOR_NO_DOB = "Alert: Please enter a valid date of birth.";
	 public static final String ERROR_FOR_DOB_LESS_THAN_18 = "Alert: Sorry you must be over 18 to enter.";
	 
	 //- About Page Statistics
	 public static final String NUMBER_OF_EMPLOYEES = "5900";	 
	 public static final String NUMBER_OF_COUNTRIES_PLAYTECH_HAS_OFFICES = "19";	 
	 public static final String GLOBAL_LICENSEES = "140";	 
	 public static final String REGULATED_JURISDICTIONS = "20";
	 
}
