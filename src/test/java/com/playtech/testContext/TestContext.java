package com.playtech.testContext;

import com.playtech.managers.BrowserManager;
import com.playtech.managers.PageObjectManager;

public class TestContext {

	private BrowserManager browserManager;
	private PageObjectManager pageObjectManager;

	public TestContext() {
		browserManager = new BrowserManager();
		pageObjectManager = new PageObjectManager(browserManager.getDriver());
	}

	public BrowserManager getBrowserManager() {
		return browserManager;
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}

}
