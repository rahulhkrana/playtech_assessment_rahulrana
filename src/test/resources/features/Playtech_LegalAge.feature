@Playtech-LegalAge
Feature: To validate range of Legal Age for accessing Playtech website.

  Background: 
    Given User opens Playtech website in browser

  @Access-Playtech-Website-With-Valid-Age @Regression
  Scenario Outline: Accessing Playtech Website with a valid age : User <Age> years
    When User is being asked to confirm the legal <Age> before entering site
    And user selects DOB "day" from the dropdown
    And user selects DOB "month" from the dropdown
    And user selects DOB "year" from the dropdown
    And user clicks on "Enter Site" button
    Then User is being redirected to Playtech Home screen

    Examples: 
      | Age |
      |  19 |
      |  18 |

  @Playtech-Welcome-Page-Legal-Age-Alert-Checks-Unhappy-Flow @Regression
  Scenario Outline: Validate welcome page legal age related alert messages : User <Age> years
    When User is being asked to confirm the legal <Age> before entering site
    And user selects DOB "day" from the dropdown
    And user selects DOB "month" from the dropdown
    And user selects DOB "year" from the dropdown
    And user clicks on "Enter Site" button
    Then user should get an error <Message> on the screen
    And User is not redirected to Playtech Home screen

    Examples: 
      | Age | Message                                      |
      |  00 | "Alert: Please enter a valid date of birth." |
      |  17 | "Alert: Sorry you must be over 18 to enter." |