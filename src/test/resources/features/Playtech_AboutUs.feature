@Playtech-AboutUs
Feature: To validate Number of Employees, Offices, Licenses and Regulated Jurisdictions from Playtech's About Us page

Background:
	Given User opens Playtech website in browser

@AboutUs-Verify-Stats @Regression
Scenario Outline: Verification of Number of Employees, Offices, Licenses and Regulated Jurisdictions when the user logs in and navigates to "About us" page
	When User is being asked to confirm the legal <Age> before entering site
	And user selects DOB "day" from the dropdown
	And user selects DOB "month" from the dropdown
	And user selects DOB "year" from the dropdown
	And user clicks on "Enter Site" button  
	Then User is being redirected to Playtech Home screen
	When user navigates to "About Us" page
	Then user should validate the "Number of Employees" successfully
	And user should validate the "Number of countries Playtech has offices" successfully
	And user should validate the "Global licensees" successfully
	And user should validate the "Regulated Jurisdictions" successfully
	
Examples:
	|Age|
	|19 |